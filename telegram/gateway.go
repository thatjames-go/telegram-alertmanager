package telegram

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"reflect"
	"strings"
	"telegram-alertmanager/config"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	baseUrl = "https://api.telegram.org/bot%s/%s"
	client  = new(http.Client)
)

//errors
var (
	ErrTelegramApiNoDataRx = errors.New("read no data from telegram api server")
	ErrTelegramApiReject   = errors.New("telegram api server rejected request")
)

type Gateway struct {
	callbackUpdateChan chan *CallbackUpdate
	botToken           string
}

func NewGateway(botToken string) (*Gateway, error) {
	gateway := &Gateway{
		botToken: botToken,
	}

	if webHookUrl := config.Configuration.Telegram.Webhook; webHookUrl != "" {
		log.Info("setup webhook for url ", webHookUrl)
		if err := setupWebHook(botToken, config.Configuration.Telegram.Webhook); err != nil {
			return nil, err
		}
	} else {
		log.Info("falling back to polling update")
		go gateway.setupUpdatePoll()
	}

	return gateway, nil
}

func (z *Gateway) RegisterUpdateHook(c chan *CallbackUpdate) {
	z.callbackUpdateChan = c
}

func (z *Gateway) SendMessage(msg *Message) error {
	request := &SendMessageRequest{
		Message: msg,
	}
	return sendTelegramRequest(z.botToken, request, msg)
}

func (z *Gateway) SendKeyboardUpdate(editRequest *EditMessageRequest) error {
	return sendTelegramRequest(z.botToken, editRequest, nil)
}

func printMap(indent string, m map[string]interface{}) {
	for key, value := range m {
		switch val := value.(type) {
		case map[string]interface{}:
			fmt.Printf("%s%s (object):\n", indent, key)
			printMap(indent+"  ", val)
		case map[string]string:
			for key, value := range val {
				fmt.Printf("%s%s - %v\n", indent, key, value)
			}
		default:
			fmt.Printf("%s%s (%T) - %v\n", indent, key, value, value)
		}
	}
}

func (z *Gateway) setupUpdatePoll() {
	timer := time.NewTicker(time.Second * 5)
	updateRequest := &UpdateRequest{
		AllowedUpdates: []string{"callback_query"},
	}
	for range timer.C {
		var updates []*CallbackUpdate
		if err := sendTelegramRequest(z.botToken, updateRequest, &updates); err != nil {
			log.Error("getUpdate err: ", err.Error())
			continue
		} else {
			for _, update := range updates {
				z.callbackUpdateChan <- update
				updateRequest.Offset = update.UpdateId + 1
			}
		}
	}
}

func sendTelegramRequest(botToken string, request TelegramRequest, i interface{}) error {
	url := fmt.Sprintf(baseUrl, botToken, request.TelegramMethod())
	logRecord := log.Fields{"method": request.TelegramMethod(), "http_method": request.HttpMethod()}
	var bodyBuff *bytes.Buffer
	if params := request.Parameters(); params != nil {
		bodyBuff = new(bytes.Buffer)
		if err := json.NewEncoder(bodyBuff).Encode(params); err != nil {
			return err
		}
		logRecord["request_body"] = bodyBuff.String()
	}
	req, err := http.NewRequest(request.HttpMethod(), url, bodyBuff)
	if err != nil {
		return err
	}

	if bodyBuff != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	} else {
		logRecord["status_code"] = resp.StatusCode
		logRecord["status"] = resp.Status
		if resp.StatusCode > 399 {
			log.WithFields(logRecord).Error("telegram request")
			return errors.New(resp.Status)
		}
	}
	if resp.ContentLength > 0 {
		defer resp.Body.Close()
		if i != nil {
			respBuff := new(bytes.Buffer)
			if _, err := io.Copy(respBuff, resp.Body); err != nil {
				return err
			}
			logRecord["response_body"] = respBuff.String()
			var result TelegramResult
			if err := json.NewDecoder(respBuff).Decode(&result); err != nil {
				return err
			} else if !result.Ok {
				log.WithFields(logRecord).Error("telegram request")
				return errors.New("telegram rejected request")
			}
			if entity, ok := result.Result.(map[string]interface{}); ok {
				bind(entity, i)
			} else {
				//dirty hack to deal with arrays, i'll do something properly when i have time
				dat, _ := json.Marshal(result.Result)
				json.Unmarshal(dat, i)
			}
		}
	}
	log.WithFields(logRecord).Debug("telegram request")
	return nil
}

func setupWebHook(botToken, webHookUrl string) error {
	//first check if there's an existing webhook
	webHookInfo, err := getWebHookInfo(botToken)
	if err != nil {
		return err
	}
	//Remove it if it is set
	if webHookInfo.URL != "" {
		if err := deleteWebHook(botToken); err != nil {
			return err
		}
	}

	//Create new webHook
	if err := createWebHook(botToken, webHookUrl); err != nil {
		return err
	}

	return nil
}

func getWebHookInfo(botToken string) (*WebHookInfo, error) {
	var webhookInfo WebHookInfo
	err := sendTelegramRequest(botToken, new(WebHookInfoRequest), &webhookInfo)
	if err != nil {
		return nil, err
	}

	return &webhookInfo, nil
}

func deleteWebHook(botToken string) error {
	return sendTelegramRequest(botToken, new(DeleteWebHookRequest), nil)
}

func createWebHook(botToken, webHookUrl string) error {
	return sendTelegramRequest(botToken, &CreateWebHookRequest{Url: webHookUrl}, nil)
}

func mapJSONPayloadToStruct(valueMap map[string]interface{}, i interface{}) {
	reflectValue := reflect.ValueOf(i)
	for reflectValue.Kind() == reflect.Ptr {
		reflectValue = reflectValue.Elem()
	}

	reflectType := reflectValue.Type()
	for i := 0; i < reflectValue.NumField(); i++ {
		field := reflectValue.Field(i)
		var lookupName string
		//Try to use the `json` tag from struct
		if tagName, ok := reflectType.Field(i).Tag.Lookup("json"); ok {
			if strings.Contains(tagName, ",") {
				lookupName = strings.Split(tagName, ",")[0]
			} else {
				lookupName = strings.ToLower(tagName)
			}
		} else {
			//If we couldn't find one, then use the snake case of the field name
			lookupName = strings.ToLower(reflectType.Field(i).Name)
		}

		switch field.Kind() {
		case reflect.Ptr:
			//create a pointer and then dereference it to avoid the nil pitfall. this makes this a struct kind now
			field = reflect.New(reflectType.Field(i).Type.Elem())
			fallthrough
		case reflect.Struct:
			//"Complex" field such as a struct. This handles embedded and declared structs
			//Grab a reference to the interface, map that badboy with the provided map, then set the field with the result
			iface := field.Interface()
			mapJSONPayloadToStruct(valueMap[lookupName].(map[string]interface{}), iface)
			reflectValue.Field(i).Set(reflect.ValueOf(iface))

		default:
			//We should be at the "bottom" of the type tree here, check to see if the data map has a value here first
			if val, ok := valueMap[lookupName]; ok {
				//Because this is usually behind a JSON payload, and because JSON has no notion of ints, we need to force the go runtime to "cast" this reflect value to an int
				if reflect.ValueOf(val).Kind() == reflect.Float64 && field.Type().Kind() == reflect.Int64 {
					field.Set(reflect.ValueOf(int64(reflect.ValueOf(val).Float())))
				} else {
					field.Set(reflect.ValueOf(val))
				}
			}
		}
	}
}
