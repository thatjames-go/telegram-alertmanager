package telegram

import "net/http"

type SendMessageRequest struct {
	*Message
}

func (z *SendMessageRequest) HttpMethod() string {
	return http.MethodGet
}

func (z *SendMessageRequest) TelegramMethod() string {
	return "sendMessage"
}

func (z *SendMessageRequest) Parameters() interface{} {
	return z
}

type EditMessageRequest struct {
	ChatId          interface{} `json:"chat_id,omitempty"`
	MessageId       int64       `json:"message_id,omitempty"`
	InlineMessageId string      `json:"inline_message_id,omitempty"`
	Text            string      `json:"text,omitempty"`
	ParseMode       string      `json:"parse_mode,omitempty"`
	ReplyMarkup     interface{} `json:"reply_markup,omitempty"`
}

func NewEditMessageRequestFromMessage(msg *Message) *EditMessageRequest {
	return &EditMessageRequest{
		ChatId:      msg.Chat.Id,
		MessageId:   msg.MessageId,
		Text:        msg.Text,
		ParseMode:   msg.ParseMode,
		ReplyMarkup: msg.ReplyMarkup,
	}
}

func (z *EditMessageRequest) HttpMethod() string {
	return http.MethodPost
}

func (z *EditMessageRequest) TelegramMethod() string {
	return "editMessageText"
}

func (z *EditMessageRequest) Parameters() interface{} {
	return z
}

type UpdateRequest struct {
	Offset         int64    `json:"offset"`
	AllowedUpdates []string `json:"allowed_updates"`
}

func (z *UpdateRequest) HttpMethod() string {
	return http.MethodGet
}

func (z *UpdateRequest) TelegramMethod() string {
	return "getUpdates"
}

func (z *UpdateRequest) Parameters() interface{} {
	return z
}

type WebHookInfoRequest struct {
}

func (z *WebHookInfoRequest) HttpMethod() string {
	return http.MethodGet
}

func (z *WebHookInfoRequest) TelegramMethod() string {
	return "getWebHookInfo"
}

func (z *WebHookInfoRequest) Parameters() interface{} {
	return nil
}

type DeleteWebHookRequest struct {
	DropPendingUpdates bool `json:"drop_pending_updates"`
}

func (z *DeleteWebHookRequest) HttpMethod() string {
	return http.MethodPost
}

func (z *DeleteWebHookRequest) TelegramMethod() string {
	return "deleteWebhook"
}

func (z *DeleteWebHookRequest) Parameters() interface{} {
	return z
}

type CreateWebHookRequest struct {
	Url                string   `json:"url"`
	AllowedUpdates     []string `json:"allowed_updates,omitempty"`
	DropPendingUpdates bool     `json:"drop_pending_updates"`
}

func (z *CreateWebHookRequest) HttpMethod() string {
	return http.MethodPost
}

func (z *CreateWebHookRequest) TelegramMethod() string {
	return "setWebhook"
}

func (z *CreateWebHookRequest) Parameters() interface{} {
	return z
}
