package telegram

import (
	"fmt"
	"reflect"
	"strings"
	"time"
)

type mappableEntity map[string]interface{}

func bind(m mappableEntity, i interface{}) {
	reflectValue := reflect.ValueOf(i)
	for reflectValue.Kind() == reflect.Ptr {
		reflectValue = reflectValue.Elem()
	}

	reflectType := reflectValue.Type()
	for i := 0; i < reflectValue.NumField(); i++ {
		field := reflectValue.Field(i)
		var lookupName string
		//Try to use the `json` tag from struct
		if tagName, ok := reflectType.Field(i).Tag.Lookup("json"); ok {
			if strings.Contains(tagName, ",") {
				lookupName = strings.Split(tagName, ",")[0]
			} else {
				lookupName = strings.ToLower(tagName)
			}
		} else {
			//If we couldn't find one, then use the snake case of the field name
			lookupName = strings.ToLower(reflectType.Field(i).Name)
		}

		switch field.Kind() {
		case reflect.Ptr:
			//create a pointer and then dereference it to avoid the nil pitfall. this makes this a struct kind now
			field = reflect.New(reflectType.Field(i).Type.Elem())
			fallthrough
		case reflect.Struct:
			//"Complex" field such as a struct. This handles embedded and declared structs
			//Grab a reference to the interface, map that badboy with the provided map, then set the field with the result
			iface := field.Interface()
			mapJSONPayloadToStruct(m[lookupName].(map[string]interface{}), iface)
			reflectValue.Field(i).Set(reflect.ValueOf(iface))

		default:
			//We should be at the "bottom" of the type tree here, check to see if the data map has a value here first
			if val, ok := m[lookupName]; ok {
				//Because this is usually behind a JSON payload, and because JSON has no notion of ints, we need to force the go runtime to "cast" this reflect value to an int
				if reflect.ValueOf(val).Kind() == reflect.Float64 && field.Type().Kind() == reflect.Int64 {
					field.Set(reflect.ValueOf(int64(reflect.ValueOf(val).Float())))
				} else {
					field.Set(reflect.ValueOf(val))
				}
			}
		}
	}
}

type TelegramResult struct {
	Ok     bool        `json:"ok"`
	Result interface{} `json:"result"`
}

type User struct {
	Id                      int64  `json:"id,omitempty"`
	IsBot                   bool   `json:"is_bot,omitempty"`
	FirstName               string `json:"first_name,omitempty"`
	LastName                string `json:"last_name,omitempty"`
	Username                string `json:"username,omitempty"`
	LanguageCode            string `json:"language_code,omitempty"`
	CanJoinGroups           bool   `json:"can_join_groups,omitempty"`
	CanReadAllGroupMessages bool   `json:"can_read_all_group_messages,omitempty"`
	SupportsInlineQueries   bool   `json:"supports_inline_queries,omitempty"`
}

type Message struct {
	MessageId   int64       `json:"message_id,omitempty"`
	ChatId      int64       `json:"chat_id,omitempty"`
	Text        string      `json:"text,omitempty"`
	ReplyMarkup interface{} `json:"reply_markup,omitempty"`
	ParseMode   string      `json:"parse_mode,omitempty"`
	Chat        *Chat       `json:"chat,omitempty"`
}

func (m *Message) String() string {
	return fmt.Sprintf("{id: %d chat_id: %d text: %s reply_markup: %+v parse_mode: %s chat: %v}", m.MessageId, m.ChatId, m.Text, m.ReplyMarkup, m.ParseMode, m.Chat)
}

type Chat struct {
	Id int64 `json:"id,omitempty"`
}

func (c *Chat) String() string {
	return fmt.Sprintf("id: %d", c.Id)
}

type CallbackUpdate struct {
	UpdateId      int64          `json:"update_id,omitempty"`
	CallbackQuery *CallbackQuery `json:"callback_query,omitempty"`
}

type CallbackQuery struct {
	ChatInstance string   `json:"chat_instance,omitempty"`
	Id           string   `json:"id,omitempty"`
	Message      *Message `json:"message,omitempty"`
	Data         string   `json:"data,omitempty"`
}

func (c *CallbackQuery) String() string {
	return fmt.Sprintf("id: %s Message: %v Data: %s", c.Id, *c.Message, c.Data)
}

type WebHook struct {
	URL                string   `json:"url,omitempty"`
	AllowedUpdates     []string `json:"allowed_updates,omitempty"`
	DropPendingUpdates bool     `json:"drop_pending_updates,omitempty"`
}

type WebHookInfo struct {
	URL                  string    `json:"url,omitempty"`
	HasCustomCertificate bool      `json:"has_custom_certificate,omitempty"`
	PendingUpdateCount   int64     `json:"pending_update_count,omitempty"`
	IpAddress            string    `json:ip_addres,omitemptys`
	LastErrorDate        time.Time `json:"last_error_date,omitempty"`
	LastErrorMessage     string    `json:"last_error_message,omitempty"`
	MaxConnections       int64     `json:"max_connections,omitempty"`
	AllowedUpdates       []string  `json:"allowed_updates,omitempty"`
}

func NewMessage(chatId int64, text string, replyMarkup interface{}) *Message {
	return &Message{
		ChatId:      chatId,
		Text:        text,
		ReplyMarkup: replyMarkup,
		ParseMode:   "HTML",
	}
}

type InlineKeyboardButton struct {
	Text         string `json:"text,omitempty"`
	CallbackData string `json:"callback_data,omitempty"`
}

type InlineKeyboardMarkup struct {
	InlineKeyboard  [][]*InlineKeyboardButton `json:"inline_keyboard,omitempty"`
	ResizeKeyboard  bool                      `json:"resize_keyboard,omitempty"`
	OneTimeKeyboard bool                      `json:"one_time_keyboard,omitempty"`
}

func NewInlineKeyboardMarkup(buttons map[string]string) *InlineKeyboardMarkup {
	btnArray := make([]*InlineKeyboardButton, 0, len(buttons))
	// btnArray := make([]string, 0, len(buttons))
	for text, callbackData := range buttons {
		btnArray = append(btnArray, &InlineKeyboardButton{
			Text:         text,
			CallbackData: callbackData,
		})
	}
	return &InlineKeyboardMarkup{
		InlineKeyboard: [][]*InlineKeyboardButton{btnArray},
	}
}
