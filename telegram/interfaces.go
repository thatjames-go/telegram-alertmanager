package telegram

type HttpMethod interface {
	HttpMethod() string
}

type TelegramMethod interface {
	TelegramMethod() string
}

type WithParameters interface {
	Parameters() interface{}
}

type TelegramRequest interface {
	HttpMethod
	TelegramMethod
	WithParameters
}
