FROM golang:latest AS builder
WORKDIR /build
COPY . .
RUN env CGO_ENABLED=0 go build -o app

FROM alpine
RUN apk add tzdata ca-certificates
COPY --from=builder /build/app /app
EXPOSE 2233
ENTRYPOINT ["/app"]
