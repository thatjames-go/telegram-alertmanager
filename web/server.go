package web

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"telegram-alertmanager/alertmanager"
	"telegram-alertmanager/config"
	"telegram-alertmanager/coordinator"
	"telegram-alertmanager/telegram"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

var (
	telegramGateway  *telegram.Gateway
	sentAlerts       = make(map[string]*telegram.EditMessageRequest)
	alertCoordinator *coordinator.AlertCoordinator
)

func LoadWebLayer(token string) error {
	var err error
	if telegramGateway, err = telegram.NewGateway(token); err != nil {
		return err
	}
	r := mux.NewRouter()
	alertCoordinator = coordinator.New(telegramGateway)
	r.HandleFunc("/alerts/{chatId}", handleAlert)
	log.Info("http server starting on", config.Configuration.Web.Listen)
	return http.ListenAndServe(config.Configuration.Web.Listen, r)
}

func handleAlert(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("recovered from panic", r)
		}
	}()
	var prometheusAlert alertmanager.AlertManagerMessage
	if err := json.NewDecoder(r.Body).Decode(&prometheusAlert); err != nil {
		log.Error("unable to decode incoming payload: ", err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Debug("serving ", len(prometheusAlert.Alerts), " alert(s)")
	chatId, err := strconv.ParseInt(mux.Vars(r)["chatId"], 10, 64)
	if err != nil {
		log.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	for i := range prometheusAlert.Alerts {
		if err := alertCoordinator.PushAlert(chatId, &prometheusAlert.Alerts[i]); err != nil {
			log.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
}
