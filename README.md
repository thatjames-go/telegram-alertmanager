# Telegram Alertmanager

A simple telegram receiver for [Prometheus's Alertmanager](https://www.prometheus.io/docs/alerting/latest/alertmanager/)

# Telegram

Based off of the [Telegram Bot API](https://core.telegram.org/bots). In order to use this application, you need to create a telegram bot. [Follow these instructions in order to do so.](https://core.telegram.org/bots#6-botfather)

# Instalation

## Docker
```
docker run --name telegram-alertmanager -p 2233:2233 --restart unless-stopped smokeycircles/telegram-alertmanager -t YOUR_TELGRAM_BOT_TOKEN_HERE
```

## Docker-Compose

```yaml
services:
  telegram-alertmanager:
    image: smokeycircles/telegram-alertmanager
    container-name: telegram-alertmanager
    ports:
      - 2233:2233
    environment:
      - TELEGRAM_BOT_TOKEN=MY_BOT_TOKEN
    restart: unless-stopped
```

# Configuration

## Flags

```
  -t string: telegram token. this is a required parameter
  -l string: web listen address. defaults to :2233
```
