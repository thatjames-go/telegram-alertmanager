package alertmanager

import "time"

type AlertManagerMessage struct {
	Alerts            []Alerts          `json:"alerts"`
	Chatid            string            `json:"chatId"`
	Commonannotations Commonannotations `json:"commonAnnotations"`
	Commonlabels      Commonlabels      `json:"commonLabels"`
	Externalurl       string            `json:"externalURL"`
	Groupkey          string            `json:"groupKey"`
	Grouplabels       Grouplabels       `json:"groupLabels"`
	Receiver          string            `json:"receiver"`
	Status            string            `json:"status"`
	Truncatedalerts   int               `json:"truncatedAlerts"`
	Version           string            `json:"version"`
}

type Annotations struct {
	Description string `json:"description"`
	Summary     string `json:"summary"`
}

type Labels struct {
	Alertname string `json:"alertname"`
	Instance  string `json:"instance"`
	Job       string `json:"job"`
	Severity  string `json:"severity"`
}

type Alerts struct {
	Annotations  Annotations `json:"annotations"`
	Endsat       time.Time   `json:"endsAt"`
	Fingerprint  string      `json:"fingerprint"`
	Generatorurl string      `json:"generatorURL"`
	Labels       Labels      `json:"labels"`
	Startsat     time.Time   `json:"startsAt"`
	Status       string      `json:"status"`
}

type Commonannotations struct {
	Description string `json:"description"`
	Summary     string `json:"summary"`
}

type Commonlabels struct {
	Alertname string `json:"alertname"`
	Instance  string `json:"instance"`
	Job       string `json:"job"`
	Severity  string `json:"severity"`
}

type Grouplabels struct {
	Alertname string `json:"alertname"`
	Job       string `json:"job"`
}

type SilenceRequest struct {
	ID        string     `json:"id,omitempty"`
	Matchers  []Matchers `json:"matchers,omitempty"`
	Startsat  string     `json:"startsAt,omitempty"`
	Endsat    string     `json:"endsAt,omitempty"`
	Createdby string     `json:"createdBy,omitempty"`
	Comment   string     `json:"comment,omitempty"`
}

type Matchers struct {
	Name    string `json:"name,omitempty"`
	Value   string `json:"value,omitempty"`
	Isregex bool   `json:"isRegex,omitempty"`
	Isequal bool   `json:"isEqual,omitempty"`
}
