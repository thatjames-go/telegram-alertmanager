package alertmanager

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"path"
	"telegram-alertmanager/config"

	log "github.com/sirupsen/logrus"
)

var client = new(http.Client)

func SendSilenceRequest(request *SilenceRequest) error {
	buff := new(bytes.Buffer)
	json.NewEncoder(buff).Encode(request)
	alertManURL, err := url.Parse(config.Configuration.AlertManager.URL)
	if err != nil {
		return err
	}
	alertManURL.Path = path.Join(alertManURL.Path, "api/v1/silences")
	logRecord := log.Fields{"url": alertManURL.String(), "request_body": buff.String()}
	req, err := http.NewRequest(http.MethodPost, alertManURL.String(), buff)
	if err != nil {
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	logRecord["status"] = resp.Status
	logRecord["status_code"] = resp.StatusCode

	if resp.ContentLength > 0 {
		defer resp.Body.Close()
		buff.Reset()
		io.Copy(buff, resp.Body)
		logRecord["response_body"] = buff.String()
	}
	log.WithFields(logRecord).Debug("alertmanager request")
	return nil
}
