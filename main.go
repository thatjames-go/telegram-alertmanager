package main

import (
	"os"
	"os/signal"
	"telegram-alertmanager/config"
	"telegram-alertmanager/web"

	log "github.com/sirupsen/logrus"
)

func main() {
	// log.SetFormatter(&log.TextFormatter{
	// 	DisableColors: true,
	// 	FullTimestamp: true,
	// })
	switch config.Configuration.Logging.Level {
	case config.LogLevelError:
		log.Info("Set Level Error")
		log.SetLevel(log.ErrorLevel)

	case config.LogLevelInfo:
		log.Info("Set Level Info")
		log.SetLevel(log.InfoLevel)

	case config.LogLevelDebug:
		log.Info("Set Level Debug")
		log.SetLevel(log.DebugLevel)
	}
	go initApp()
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Kill, os.Interrupt)
	sig := <-sigChan
	log.Info("caught signal", sig)
}

func initApp() {
	if err := web.LoadWebLayer(config.Configuration.Telegram.BotToken); err != nil {
		panic(err)
	}
}
