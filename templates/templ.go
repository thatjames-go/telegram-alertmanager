package templates

import (
	_ "embed"
	"io/ioutil"
	"os"
	"text/template"
	"time"
)

var (
	//go:embed alert_template
	mainTempl string
	Main      = template.Must(template.New("main").Funcs(template.FuncMap{"now": timeNow}).Parse(mainTempl))
)

func LoadTemplateFromFile(path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	dat, err := ioutil.ReadAll(f)
	userTempl, err := template.New("user").Funcs(template.FuncMap{"now": timeNow}).Parse(string(dat))
	if err != nil {
		return nil
	}
	Main = userTempl
	return nil
}

func timeNow(format string) string {
	return time.Now().Format(format)
}