package config

import (
	"flag"
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

var (
	Configuration *ConfigDefinition
)

//flags
var (
	configFile string
	debug      bool
)

const (
	LogLevelError LogLevel = iota //0
	LogLevelInfo                  //1
	LogLevelDebug                 //2
)

func init() {
	Configuration = &ConfigDefinition{
		Web: &WebConfig{
			Listen: ":2233",
		},
		Telegram: &TelegramConfig{},
		AlertManager: &AlertManagerConfig{
			URL: "http://localhost:9093",
		},
		Logging: &LogConfig{
			Level: LogLevelInfo,
		},
	}
	flag.StringVar(&Configuration.Telegram.BotToken, "t", "", "required: telegram bot token")
	flag.StringVar(&Configuration.Web.Listen, "l", ":2233", "optional: listening address")
	flag.StringVar(&Configuration.Telegram.Webhook, "w", "", "optional: webhook for telegram updates")
	flag.StringVar(&Configuration.AlertManager.URL, "a", "http://localhost:9093", "optional: alertmanager url")
	flag.StringVar(&configFile, "c", "", "optional: path to yaml config file")
	flag.BoolVar(&debug, "v", false, "optional: verbose printing")
	flag.Usage = usage
	flag.Parse()
	if configFile != "" {
		if f, err := os.Open(configFile); err == nil {
			var newConfig ConfigDefinition
			if err := yaml.NewDecoder(f).Decode(&newConfig); err == nil {
				*Configuration = newConfig
			}
		}
	}
	if Configuration.Telegram.BotToken == "" {
		if Configuration.Telegram.BotToken = os.Getenv("TELEGRAM_BOT_TOKEN"); Configuration.Telegram.BotToken == "" {
			flag.Usage()
			return
		}
	}
	if debug {
		Configuration.Logging.Level = LogLevelDebug
	}
}

func usage() {
	fmt.Println("telegram-alertmanager <options>")
	fmt.Println("options:")
	flag.PrintDefaults()
	os.Exit(0)
}

type ConfigDefinition struct {
	Web          *WebConfig          `yaml:"web"`
	Telegram     *TelegramConfig     `yaml:"telegram"`
	AlertManager *AlertManagerConfig `yaml:"alertmanager"`
	Logging      *LogConfig          `yaml:"logging"`
}

type WebConfig struct {
	Listen string `yaml:"listen"`
}

type TelegramConfig struct {
	Webhook  string `yaml:"webhook"`
	BotToken string `yaml:"bottoken"`
}

type AlertManagerConfig struct {
	URL string `yaml:"url"`
}

type LogLevel int

type LogConfig struct {
	Level LogLevel `yaml:"level"`
}
