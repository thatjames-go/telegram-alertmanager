package coordinator

import (
	"bytes"
	"errors"
	"fmt"
	"telegram-alertmanager/alertmanager"
	"telegram-alertmanager/telegram"
	"telegram-alertmanager/templates"
	"time"

	log "github.com/sirupsen/logrus"
)

type AlertCoordinator struct {
	telegramGateway *telegram.Gateway
	alertChan       chan *alertTask
	sentAlerts      map[string]*alertTask
	updateChan      chan *telegram.CallbackUpdate
}

type alertTask struct {
	*alertmanager.Alerts
	*telegram.Message
	Count      int
	Resolved   bool
	chatId     int64
	result     bool
	error      string
	resultChan chan *alertTask
}

func New(telegramGateway *telegram.Gateway) *AlertCoordinator {
	ac := &AlertCoordinator{
		telegramGateway: telegramGateway,
		alertChan:       make(chan *alertTask, 100),
		sentAlerts:      make(map[string]*alertTask),
		updateChan:      make(chan *telegram.CallbackUpdate),
	}

	telegramGateway.RegisterUpdateHook(ac.updateChan)

	ac.start()

	return ac
}

func (z *AlertCoordinator) PushAlert(chatId int64, alert *alertmanager.Alerts) error {
	resultChan := make(chan *alertTask)
	z.alertChan <- &alertTask{
		Alerts:     alert,
		resultChan: resultChan,
		chatId:     chatId,
	}
	result := <-resultChan
	if !result.result {
		return errors.New(result.error)
	}
	return nil
}

func (z *AlertCoordinator) start() {
	go z.processChannels()
}

func (z *AlertCoordinator) processChannels() {
	for {
		select {
		case alert := <-z.alertChan:
			z.handleAlert(alert)

		case update := <-z.updateChan:
			z.handleUpdate(update)
		}
	}
}

func (z *AlertCoordinator) handleAlert(alert *alertTask) {
	defer func() {
		alert.resultChan <- alert
	}()
	buff := new(bytes.Buffer)
	if existingAlert, ok := z.sentAlerts[alert.Labels.Alertname]; ok {
		existingAlert.Count++
		existingAlert.Status = alert.Status
		if err := templates.Main.Execute(buff, existingAlert); err != nil {
			alert.error = err.Error()
			return
		}

		editMessageRequest := telegram.NewEditMessageRequestFromMessage(existingAlert.Message)
		editMessageRequest.Text = buff.String()
		if alert.Status == "resolved" {
			editMessageRequest.ReplyMarkup = nil
		}
		if err := z.telegramGateway.SendKeyboardUpdate(editMessageRequest); err != nil {
			alert.error = err.Error()
			return
		}
		log.Debug("Sent alert with status ", alert.Status)
	} else {
		if err := templates.Main.Execute(buff, alert); err != nil {
			alert.error = err.Error()
			return
		}
		msg := telegram.NewMessage(alert.chatId, buff.String(), telegram.NewInlineKeyboardMarkup(map[string]string{"Silence Alert": alert.Labels.Alertname}))
		if err := z.telegramGateway.SendMessage(msg); err != nil {
			alert.error = err.Error()
			return
		}
		alert.Message = msg
		alert.Count = 1
		z.sentAlerts[alert.Labels.Alertname] = alert
	}
	alert.result = true
}

func (z *AlertCoordinator) handleUpdate(update *telegram.CallbackUpdate) {
	silenceRequest := &alertmanager.SilenceRequest{
		Matchers: []alertmanager.Matchers{
			alertmanager.Matchers{
				Name:  "alertname",
				Value: update.CallbackQuery.Data,
			},
		},
		Startsat:  time.Now().Format("2006-01-02T15:04:05Z07:00"),
		Endsat:    time.Now().Add(time.Hour * 2).Format("2006-01-02T15:04:05Z07:00"),
		Createdby: "Telegram Alert Bot",
		Comment:   "Alert was created by user interaction with a configured telgram bot",
	}

	if err := alertmanager.SendSilenceRequest(silenceRequest); err != nil {
		log.Error("unable to create silence:", err.Error())
		return
	}

	editMessageRequest := &telegram.EditMessageRequest{
		ChatId:    update.CallbackQuery.Message.Chat.Id,
		MessageId: update.CallbackQuery.Message.MessageId,
		Text:      fmt.Sprintf("Alert %s silenced", update.CallbackQuery.Data),
		ParseMode: update.CallbackQuery.Message.ParseMode,
	}

	if err := z.telegramGateway.SendKeyboardUpdate(editMessageRequest); err != nil {
		log.Error("unable to update text: ", err.Error())
		return
	}
	delete(z.sentAlerts, update.CallbackQuery.Data)
}
